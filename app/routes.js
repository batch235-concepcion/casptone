const { exchangeRates } = require("../src/util.js");
const express = require("express");
const router = express.Router();

router.get("/rates", (req, res) => {
  return res.status(200).send(exchangeRates);
});

router.post("/currency", (req, res) => {
  if (!req.body.hasOwnProperty("ex")) {
    return res.status(400).send({
      error: "Bad Request: missing required parameter EX",
    });
  }

  if (!req.body.hasOwnProperty("name")) {
    return res.status(400).send({
      error: "Bad Request: missing required parameter NAME",
    });
  }

  if (!req.body.hasOwnProperty("alias")) {
    return res.status(400).send({
      error: "Bad Request: missing required parameter ALIAS",
    });
  }

  let foundAlias = exchangeRates.find((result) => {
    if (
      forex.alias === req.body.alias ||
      forex.name === req.body.name ||
      forex.ex === req.body.ex
    )
      return result;
  });

  if (!foundAlias) {
    return res.status(403).send({
      forbidden: "Duplicate Alias",
    });
  }
});

module.exports = router;
